-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2019 at 08:35 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `facial_works`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `account_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `user_level` enum('Administrator','Branch Secretary','Customer') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`account_id`, `username`, `password`, `name`, `address`, `contact_number`, `user_level`) VALUES
(1, 'admin', 'admin', 'Jane Nebre', 'Villasis, Pangasinan', '09664690384', 'Administrator'),
(2, 'villasis', 'villasis', 'Villasis Staff', 'Villasis, Pangasinan', '09664690384', 'Branch Secretary'),
(3, 'urdaneta', 'urdaneta', 'Urdaneta Staff', 'Urdaneta Pangasinan', '09664690384', 'Branch Secretary');

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `announcement_id` int(11) NOT NULL,
  `announcement_title` varchar(255) DEFAULT NULL,
  `announcement_description` text,
  `announcement_image` varchar(255) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`announcement_id`, `announcement_title`, `announcement_description`, `announcement_image`, `account_id`, `date_created`) VALUES
(4, 'What is Lorem Ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, 1, '2018-12-01 16:43:55'),
(5, 'Why do we use it?', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', NULL, 1, '2018-12-01 16:44:12'),
(6, 'Where does it come from?', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\n\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', NULL, 1, '2018-12-01 16:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `customer_name` text,
  `customer_address` text,
  `customer_birthday` varchar(255) DEFAULT NULL,
  `customer_contact` varchar(255) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `customer_address`, `customer_birthday`, `customer_contact`, `is_deleted`, `date_created`, `date_updated`) VALUES
(1, 'Joel Jed Macaraig', 'Rosales, Pangasinan', '1993-06-17', '09664690384', 0, '2019-02-02 05:13:28', '2019-02-02 06:02:54'),
(7, 'Jeive Lafeguera', 'Villasis, Pangasinan', '1993-06-12', '09665968459', 0, '2019-02-01 23:17:49', '2019-02-02 06:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL,
  `action` text,
  `message` text,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `action`, `message`, `date_created`) VALUES
(16, 'INSERT INTO announcements (announcement_title, announcement_description, announcement_image, account_id, date_created) VALUES (What is Lorem Ipsum?, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum., NULL, 1, 2018-12-01 16:43:55)', 'Account ID: 1 added new announcement.', '2018-12-01 16:43:55'),
(17, 'INSERT INTO announcements (announcement_title, announcement_description, announcement_image, account_id, date_created) VALUES (Why do we use it?, It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \\Content here, content here\\, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \\lorem ipsum\\ will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)., NULL, 1, 2018-12-01 16:44:12)', 'Account ID: 1 added new announcement.', '2018-12-01 16:44:12'),
(18, 'INSERT INTO announcements (announcement_title, announcement_description, announcement_image, account_id, date_created) VALUES (Where does it come from?, Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \\de Finibus Bonorum et Malorum\\ (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \\Lorem ipsum dolor sit amet..\\, comes from a line in section 1.10.32.\\n\\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \\de Finibus Bonorum et Malorum\\ by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham., NULL, 1, 2018-12-01 16:46:23)', 'Account ID: 1 added new announcement.', '2018-12-01 16:46:23'),
(19, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (Sample23, NULL, 300, 3, 5, 945, 2018-12-01 20:27:21, 1)', 'Account ID: 1 added new transaction.', '2018-12-01 20:27:21'),
(20, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (Sample23, sample desc 23, 300, 3, 5, 945, 2018-12-01 20:29:38, 1)', 'Account ID: 1 added new transaction.', '2018-12-01 20:29:38'),
(21, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (service 1, 2, 300.00, 1, 0, 300, 2018-12-01 20:29:59, 1)', 'Account ID: 1 added new transaction.', '2018-12-01 20:29:59'),
(22, 'UPDATE products SET product_title = Product 1, product_description = Description 1, product_unit = , product_price = 2322, product_stocks = 15 WHERE product_id = 1', 'Account ID: 1 updates the product.', '2018-12-01 21:56:55'),
(23, 'UPDATE products SET product_title = Product 2, product_description = Description 2, product_unit = , product_price = 219, product_stocks = 15 WHERE product_id = 2', 'Account ID: 1 updates the product.', '2018-12-01 21:58:25'),
(24, 'UPDATE products SET product_title = Product 1, product_description = Description 1, product_unit = , product_price = 20, product_stocks = 15 WHERE product_id = 1', 'Account ID: 1 updates the product.', '2018-12-01 21:58:34'),
(25, 'UPDATE products SET product_title = Product 3, product_description = Description 3, product_unit = , product_price = 300.00, product_stocks = 12 WHERE product_id = 3', 'Account ID: 1 updates the product.', '2018-12-01 21:58:48'),
(26, 'UPDATE products SET product_title = Product 4, product_description = sampleDesc por 2, product_unit = 4444, product_price = 300.00, product_stocks = 15 WHERE product_id = 4', 'Account ID: 1 updates the product.', '2018-12-02 06:35:09'),
(27, 'UPDATE services SET service_title = service 1, service_description = desc, service_unit = 1231541231, service_price = 300.00 WHERE service_id = 1', 'Account ID: 1 updates the service.', '2018-12-02 06:58:13'),
(28, 'UPDATE transactions SET transaction_title = Sample23, transaction_unit = sample, transaction_price = , transaction_items = NULL, transaction_discount = NULL, transaction_total = NULL, date_created = 2018-12-02 07:22:17, account_id = 1 WHERE transaction_id = 1', 'Account ID: 1 updates the transaction.', '2018-12-02 07:22:17'),
(29, 'UPDATE transactions SET transaction_title = Sample23, transaction_unit = sample, transaction_price = 300, transaction_items = NULL, transaction_discount = 5, transaction_total = 945, date_created = 2018-12-02 07:24:38, account_id = 1 WHERE transaction_id = 1', 'Account ID: 1 updates the transaction.', '2018-12-02 07:24:38'),
(30, 'UPDATE transactions SET transaction_title = Sample23, transaction_unit = sample, transaction_price = 300.00, transaction_items = 3, transaction_discount = 5, transaction_total = 945, date_created = 2018-12-02 07:25:38, account_id = 1 WHERE transaction_id = 1', 'Account ID: 1 updates the transaction.', '2018-12-02 07:25:38'),
(31, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (service 1, , 300.00, 1, 0, 300, 2018-12-02 07:29:26, 1)', 'Account ID: 1 added new transaction.', '2018-12-02 07:29:26'),
(32, 'INSERT INTO products (product_title, product_description, product_unit, product_price, product_stocks, date_created) VALUES (Samplepor, sampleDesc por, NULL, 300, 15, 2018-12-02 08:38:04)', 'Account ID: 1 added new product.', '2018-12-02 08:38:05'),
(33, 'INSERT INTO products (product_title, product_description, product_unit, product_price, product_stocks, date_created) VALUES (qwe, qwe, , 1231, 1, 2018-12-02 08:44:03)', 'Account ID: 1 added new product.', '2018-12-02 08:44:03'),
(34, 'INSERT INTO products (product_title, product_description, product_unit, product_price, product_stocks, date_created) VALUES (qwe, qwe, , 123, 1, 2018-12-02 08:44:35)', 'Account ID: 1 added new product.', '2018-12-02 08:44:36'),
(35, 'INSERT INTO services (service_title, service_description, service_unit, service_price, date_created) VALUES (Derma, ioqwjeqw, 1 session, 400, 2018-12-02 08:51:05)', 'Account ID: 1 added new service.', '2018-12-02 08:51:06'),
(36, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (service 1, , 300.00, 1, 0, 300, 2018-12-17 20:36:30, 1)', 'Account ID: 1 added new transaction.', '2018-12-17 20:36:30'),
(37, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (service 1, , 300.00, 1, 0, 300, 2018-12-17 20:47:30, 1)', 'Account ID: 1 added new transaction.', '2018-12-17 20:47:30'),
(38, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (service 1, , 300.00, 1, 0, 300, 2018-12-17 20:47:55, 1)', 'Account ID: 1 added new transaction.', '2018-12-17 20:47:55'),
(39, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, date_created, account_id) VALUES (Derma, , 400.00, 1, 0, 400, 2018-12-17 20:48:08, 1)', 'Account ID: 1 added new transaction.', '2018-12-17 20:48:08'),
(40, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:04:09'),
(41, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:07:40'),
(42, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:08:12'),
(43, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:10:18'),
(44, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 updates the transaction.', '2018-12-17 21:17:01'),
(45, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:17:01'),
(46, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 updates the transaction.', '2018-12-17 21:18:18'),
(47, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:18:18'),
(48, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 updates the transaction.', '2018-12-17 21:18:34'),
(49, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:18:34'),
(50, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 updates the transaction.', '2018-12-17 21:18:54'),
(51, 'SELECT * FROM products WHERE product_title = Product 1 AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2018-12-17 21:18:54'),
(52, 'SELECT * FROM products WHERE product_title = Samplepor', 'Account ID: 1 add new stocks.', '2018-12-17 21:25:17'),
(53, 'SELECT * FROM products WHERE product_title = Samplepor', 'Account ID: 1 add new stocks.', '2018-12-17 21:25:47'),
(54, 'SELECT * FROM products WHERE product_title = Samplepor', 'Account ID: 1 add new stocks.', '2018-12-17 21:26:11'),
(55, 'SELECT * FROM products WHERE product_title = Samplepor', 'Account ID: 1 add new stocks.', '2018-12-17 21:26:22'),
(56, 'SELECT * FROM products WHERE product_title = Samplepor', 'Account ID: 1 add new stocks.', '2018-12-17 21:26:31'),
(57, 'SELECT * FROM products WHERE product_title = Sunblock Cream AND product_stocks >= 3', 'Account ID: 1 updates the transaction.', '2019-01-27 19:40:46'),
(58, 'SELECT * FROM products WHERE product_title = Sunblock Cream AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2019-01-27 19:40:46'),
(59, 'SELECT * FROM products WHERE product_title = qweqw', 'Account ID: 1 added new product.', '2019-01-27 19:46:37'),
(60, 'SELECT * FROM products WHERE product_title = qweqw', 'Account ID: 1 added new product.', '2019-01-27 19:48:56'),
(61, 'SELECT * FROM products WHERE product_title = Samplepor', 'Account ID: 1 added new product.', '2019-01-27 19:49:29'),
(62, 'SELECT * FROM products WHERE product_title = qwe', 'Account ID: 1 added new product.', '2019-01-27 19:57:40'),
(63, 'SELECT * FROM products WHERE product_title = qwe', 'Account ID: 1 add new stocks.', '2019-01-27 20:07:06'),
(64, 'SELECT * FROM products WHERE product_title = qwe', 'Account ID: 1 add new stocks.', '2019-01-27 20:39:25'),
(65, 'SELECT * FROM products WHERE product_title = qwe', 'Account ID: 1 add new stocks.', '2019-01-27 20:40:01'),
(66, 'SELECT * FROM products WHERE product_title = Rejuvinating Cream AND product_stocks >= 10', 'Account ID: 1 updates the transaction.', '2019-01-27 21:30:44'),
(67, 'SELECT * FROM products WHERE product_title = Rejuvinating Cream AND product_stocks >= 10', 'Account ID: 1 added new transaction.', '2019-01-27 21:30:45'),
(68, 'INSERT INTO services (service_title, service_description, service_unit, service_price, date_created) VALUES (Derma, ioqwjeqw, 1 session, 852, 2019-02-01 20:23:17)', 'Account ID: 1 added new service.', '2019-02-01 20:23:17'),
(69, 'INSERT INTO announcements (announcement_title, announcement_description, announcement_image, account_id, date_created) VALUES (sample, 1283u1289jqwiojeiqow, NULL, 1, 2019-02-01 21:08:45)', 'Account ID: 1 added new announcement.', '2019-02-01 21:08:45'),
(70, 'INSERT INTO announcements (announcement_title, announcement_description, announcement_image, account_id, date_created) VALUES (qwe, qwe, NULL, 1, 2019-02-01 22:01:07)', 'Account ID: 1 added new announcement.', '2019-02-01 22:01:08'),
(71, 'DELETE FROM announcements WHERE announcement_id = 8', 'Account ID: 1 deletes the announcement.', '2019-02-01 22:01:11'),
(72, 'INSERT INTO announcements (customer_name, customer_address, customer_birthday, customer_contact, date_created) VALUES (Joel Jed K. Macaraig, Rosales, Pangasinan, 06-17-1993, 09664690384, 2019-02-01 22:17:23)', 'Account ID: 1 added new announcement.', '2019-02-01 22:17:23'),
(73, 'INSERT INTO customers (customer_name, customer_address, customer_birthday, customer_contact, date_created) VALUES (Joel Jed K. Macaraig, Rosales, Pangasinan, 06-17-1993, 09664690384, 2019-02-01 22:17:56)', 'Account ID: 1 added new customer.', '2019-02-01 22:17:56'),
(74, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig', 'Account ID: 1 added new customer.', '2019-02-01 22:22:57'),
(75, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig AND customer_contact = 096646903842', 'Account ID: 1 added new customer.', '2019-02-01 22:24:45'),
(76, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig AND customer_contact = 096646903842', 'Account ID: 1 added new customer.', '2019-02-01 22:26:00'),
(77, NULL, 'Account ID: 1 tried to add new customer but customer already existed.', '2019-02-01 22:26:04'),
(78, NULL, 'Account ID: 1 tried to add new customer but customer already existed.', '2019-02-01 22:26:26'),
(79, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig OR customer_contact = 09664690384', 'Account ID: 1 tried to add new customer but customer already existed.', '2019-02-01 22:27:18'),
(80, 'UPDATE customers SET customer_name = Joel Jed Macaraig, customer_address = Rosales, Pangasinan, customer_birthday = 06-17-1993, customer_contact = 09664690384 WHERE customer_id = 1', 'Account ID: 1 updates the customers information of Joel Jed Macaraig.', '2019-02-01 22:32:33'),
(81, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig123123131 OR customer_contact = 09664690384', 'Account ID: 1 tried to add new customer but customer already existed.', '2019-02-01 22:33:23'),
(82, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig123123131 OR customer_contact = 096646903842', 'Account ID: 1 added new customer.', '2019-02-01 22:33:27'),
(83, 'UPDATE customers SET is_deleted = 1 WHERE customer_id = 6', 'Account ID: 1 deletes a customer.', '2019-02-01 22:36:44'),
(84, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig123123131 OR customer_contact = 096646903842', 'Account ID: 1 restores the data of our customer named: Joel Jed K. Macaraig123123131.', '2019-02-01 22:41:35'),
(85, 'SELECT * FROM customers WHERE customer_name = Joel Jed K. Macaraig123123131 OR customer_contact = 096646903842', 'Account ID: 1 tried to add new customer but customer already existed.', '2019-02-01 22:41:43'),
(86, 'SELECT * FROM customers WHERE customer_name = qwe OR customer_contact = 09665968459', 'Account ID: 1 added new customer.', '2019-02-01 23:17:49'),
(87, 'UPDATE customers SET customer_name = Jeive, customer_address = qwe, customer_birthday = 1993-06-12, customer_contact = 09665968459 WHERE customer_id = 7', 'Account ID: 1 updates the customers information of Jeive.', '2019-02-01 23:33:40'),
(88, 'UPDATE customers SET customer_name = Jeive Lafeguera, customer_address = Villasis, Pangasinan, customer_birthday = 1993-06-12, customer_contact = 09665968459 WHERE customer_id = 7', 'Account ID: 1 updates the customers information of Jeive Lafeguera.', '2019-02-01 23:33:53'),
(89, 'SELECT * FROM products WHERE product_title = Facial Works Soap (papaya) AND product_stocks >= 3', 'Account ID: 1 updates the transaction.', '2019-02-02 00:20:56'),
(90, 'SELECT * FROM products WHERE product_title = Facial Works Soap (papaya) AND product_stocks >= 3', 'Account ID: 1 added new transaction.', '2019-02-02 00:20:57'),
(91, 'SELECT * FROM products WHERE product_title = Facial Works Soap (papaya) AND product_stocks >= 5', 'Account ID: 1 updates the transaction.', '2019-02-02 00:21:25'),
(92, 'SELECT * FROM products WHERE product_title = Facial Works Soap (papaya) AND product_stocks >= 5', 'Account ID: 1 added new transaction.', '2019-02-02 00:21:26'),
(93, 'SELECT * FROM products WHERE product_title = Bleaching Cream AND product_stocks >= 1', 'Account ID: 1 updates the transaction.', '2019-02-02 00:25:31'),
(94, 'INSERT INTO transactions (transaction_title, transaction_unit, transaction_price, transaction_items, transaction_discount, transaction_total, transaction_type, date_created, account_id, customer_id) VALUES (Upper Lip, , 300.00, 1, 0, 300, sold, 2019-02-02 00:25:57, 1, 7)', 'Account ID: 1 added new transaction.', '2019-02-02 00:25:58'),
(95, 'SELECT * FROM products WHERE product_title = Facial Works Soap (papaya) AND product_stocks >= 1', 'Account ID: 1 updates the transaction.', '2019-02-02 00:38:24'),
(96, 'SELECT * FROM products WHERE product_title = Facial Works Soap (papaya) AND product_stocks >= 1', 'Account ID: 1 added new transaction.', '2019-02-02 00:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_title` varchar(255) DEFAULT NULL,
  `product_description` varchar(255) DEFAULT NULL,
  `product_unit` varchar(255) DEFAULT NULL,
  `product_price` double(10,2) DEFAULT NULL,
  `product_stocks` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title`, `product_description`, `product_unit`, `product_price`, `product_stocks`, `date_created`, `date_updated`) VALUES
(9, 'Facial Works Soap (papaya)', 'PAPAYA Whitening Soap helps to provide you a natural, safe and result-oriented ways to soothe and whiten your skin.', '1 pcs', 200.00, 291, '2019-01-28 02:27:48', '2019-02-02 07:38:24'),
(10, 'Bleaching Cream', 'Skin lightening products -- also known as bleaching creams, whiteners, skin brighteners, or fading creams -- work by reducing a pigment called melanin in the skin. Most people who use lighteners do so to treat skin problems such as freckles, age spots, ac', '12 oz', 265.00, 199, '2019-01-28 02:27:53', '2019-02-02 07:25:31'),
(11, 'Sunblock Cream', 'Sunscreen, also known as sunblock, sun cream or suntan lotion, is a lotion, spray, gel or other topical product that absorbs or reflects some of the sun\'s ultraviolet (UV) radiation and thus helps protect against sunburn.', '30 g', 299.00, 147, '2019-01-28 02:27:55', '2019-01-28 02:40:46'),
(12, 'Rejuvinating Cream', 'A rejuvenating skin cream can firm your skin if it uses the right ingredients. If your cream includes the ingredient deanol then that means it has the ability to stimulate collagen production and keep your skin tight and youthful. Your collagen is the mag', '75 ml', 150.00, 240, '2019-01-28 02:27:57', '2019-01-28 04:30:44'),
(13, 'Toner', 'In cosmetics, skin toner or simply toner refers to a lotion or wash designed to cleanse the skin and shrink the appearance of pores, usually used on the face. Toners can be applied to the skin in different ways', '125 ml', 150.00, 150, '2019-01-28 02:28:01', '2019-01-28 02:28:02'),
(14, 'Roll-on', NULL, '40 ml', 250.00, 300, '2019-01-28 02:28:03', '2019-01-28 02:28:04'),
(15, 'Lotion', 'A lotion is a low-viscosity topical preparation intended for application to unbroken skin. ... While lotion may be used as a medicine delivery system, many lotions, especially hand lotions and body lotions are meant instead to simply smooth, moisturize an', '295 ml', 250.00, 250, '2019-01-28 02:28:05', '2019-01-28 02:28:07'),
(16, 'Sunblock Collagen', 'Just like sunblock but has collagen', '60 ml', 100.00, 150, '2019-01-28 02:28:08', '2019-01-28 02:28:10'),
(17, 'Underarm Whitening', 'Underarm Whiteners. There are also a wide range of skin lightening and bleaching creams on the market, which are designed to fade out those dark patches under your arms. ', '60 ml', 320.00, 100, '2019-01-28 02:28:11', '2019-01-28 02:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_title` varchar(255) DEFAULT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `service_unit` varchar(255) DEFAULT NULL,
  `service_price` double(10,2) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_title`, `service_description`, `service_unit`, `service_price`, `date_created`, `date_updated`) VALUES
(1, 'Upper Lip', 'Permanent laser hair removal', '1 Session', 300.00, '2018-12-01 12:32:59', '2019-02-02 03:15:50'),
(2, 'Lower Lip', 'Permanent laser hair removal', '1 Session', 400.00, '2018-12-02 08:51:05', '2019-02-02 03:15:54'),
(3, 'UL with LLip', 'Permanent laser hair removal', '2 Session', 500.00, '2018-12-02 08:51:05', '2019-02-02 03:22:01'),
(4, 'Underarms', 'Permanent laser hair removal', '1 Session', 400.00, '2018-12-02 08:51:05', '2019-02-02 03:22:02'),
(5, 'Beard', 'Permanent laser hair removal', '1 Session', 1000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:03'),
(6, 'Side Beard', 'Permanent laser hair removal', '1 Session', 500.00, '2018-12-02 08:51:05', '2019-02-02 03:22:04'),
(7, 'Chest', 'Permanent laser hair removal', '1 Session', 1500.00, '2018-12-02 08:51:05', '2019-02-02 03:22:04'),
(8, 'Full Back', 'Permanent laser hair removal', '1 Session', 3000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:05'),
(9, 'Half Back', 'Permanent laser hair removal', '1 Session', 1200.00, '2018-12-02 08:51:05', '2019-02-02 03:22:06'),
(10, 'Bikini Line ', 'Permanent laser hair removal', '1 Session', 1000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:06'),
(11, 'Brazilian', 'Permanent laser hair removal', '1 Session', 1000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:07'),
(12, 'Labia Strip', 'Permanent laser hair removal', '1 Session', 1000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:08'),
(13, 'Stomach Strip', 'Permanent laser hair removal', '1 Session', 1000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:08'),
(14, 'Half Arms', 'Permanent laser hair removal', '1 Session', 1000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:09'),
(15, 'Full Arms', 'Permanent laser hair removal', '1 Session', 2000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:10'),
(16, 'Lower Legs', 'Permanent laser hair removal', '1 Session', 1500.00, '2018-12-02 08:51:05', '2019-02-02 03:22:10'),
(17, 'Whole Legs', 'Permanent laser hair removal', '1 Session', 3000.00, '2018-12-02 08:51:05', '2019-02-02 03:22:11'),
(18, 'Spider Veins Removal', NULL, '1 Session', 600.00, '2018-12-02 08:51:05', '2019-02-02 03:22:12'),
(19, 'Pigment Lesion', NULL, '1 Session', 600.00, '2018-12-02 08:51:05', '2019-02-02 03:22:15'),
(20, 'Acne Clearance', NULL, '1 Session', 600.00, '2018-12-02 08:51:05', '2019-02-02 03:22:14'),
(21, 'Facial Rejuvenation', NULL, '1 Session', 600.00, '2018-12-02 08:51:05', '2019-02-02 03:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` int(11) NOT NULL,
  `transaction_title` varchar(255) DEFAULT NULL,
  `transaction_unit` varchar(255) DEFAULT NULL,
  `transaction_price` double(10,2) DEFAULT NULL,
  `transaction_items` int(11) DEFAULT NULL,
  `transaction_discount` double(10,2) DEFAULT NULL,
  `transaction_total` double(10,0) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `account_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `transaction_title`, `transaction_unit`, `transaction_price`, `transaction_items`, `transaction_discount`, `transaction_total`, `transaction_type`, `date_created`, `date_updated`, `account_id`, `customer_id`) VALUES
(16, 'Sunblock Cream', '30 g', 299.00, 3, 0.00, 897, 'sold', '2019-01-27 19:40:46', '2019-02-01 23:12:15', 1, 1),
(20, 'Rejuvinating Cream', '75 ml', 150.00, 10, 0.00, 1500, 'sold', '2019-01-27 21:30:45', '2019-02-01 23:12:17', 1, 7),
(22, 'Facial Works Soap (papaya)', '1 pcs', 200.00, 3, 5.00, 1000, 'sold', '2019-02-02 00:20:56', NULL, 1, 1),
(23, 'Facial Works Soap (papaya)', '1 pcs', 200.00, 5, 0.00, 1000, 'sold', '2019-02-02 00:21:25', '2019-02-01 23:26:04', 1, 1),
(25, 'Facial Works Soap (papaya)', '1 pcs', 200.00, 1, 0.00, 200, 'sold', '2019-02-02 00:38:24', NULL, 1, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`announcement_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `announcement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
