<?php
namespace modules;
class ServiceModule{
	private $serviceService = null;
	public function __construct(){
		$this->serviceService = new \services\ServiceService();
	}

	public function getServicesModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->serviceService->getServicesServiceFunction();
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function getOneServicesModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
				"service_id"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->serviceService->getOneServicesServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function newServicesModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"service_title" 		=> "required",
			"service_description"	=> "required",
			"service_unit" 			=> "required",
			"service_price"		  	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->serviceService->newServicesServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function updateServicesModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"service_id"		  	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->serviceService->updateServicesServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}
}
