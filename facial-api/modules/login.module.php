<?php
namespace modules;
class LoginModule{
	private $loginService = null;
	public function __construct(){
		$this->loginService = new \services\loginService();
	}

	public function loginUserFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"username"		  => "required",
				"password"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->loginService->loginServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function testModuleFunction2($params){
		$isDataValid = \Gump::is_valid($params, [
				"id"		  => "required|max_len,1",
				"name"		  => "required|max_len,8",
		]);

		if($isDataValid === true){
			$resultFromService = $this->bookService->testServiceFunction1($params);
			return ['resposeCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['resposeCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}
}
