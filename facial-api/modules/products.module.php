<?php
namespace modules;
class ProductModule{
	private $productsService = null;
	public function __construct(){
		$this->productsService = new \services\ProductsService();
	}

	public function getProductsModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->productsService->getProductsServiceFunction();
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function getOneProductsModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
				"product_id"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->productsService->getOneProductsServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function newProductsModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"product_title" 		=> "required",
			"product_description"	=> "required",
			"product_price"		  	=> "required",
			"product_stocks"	  	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->productsService->newProductsServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function updateProductsModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"product_id"		  		=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->productsService->updateProductsServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}
}
