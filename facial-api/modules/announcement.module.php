<?php
namespace modules;
class AnnouncementModule{
	private $announcementService = null;
	public function __construct(){
		$this->announcementService = new \services\AnnouncementService();
	}

	public function getAnnouncementModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->announcementService->getAnnouncementServiceFunction();
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
}

	public function newAnnouncementModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			    => "required",
			"token"		  			    => "required",
			"announcement_title" 		=> "required",
			"announcement_description"	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->announcementService->newAnnouncementServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function updateAnnouncementModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"announcement_id"	  	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->announcementService->updateAnnouncementServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
    }
    
    public function deleteAnnouncementModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"announcement_id"	  	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->announcementService->deleteAnnouncementServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}
}
