<?php
namespace modules;
class TransactionModule{
	private $transactionService = null;
	public function __construct(){
		$this->transactionService = new \services\TransactionService();
	}

	public function getTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->getTransactionServiceFunction();
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function getOneTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
				"transaction_id"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->getOneTransactionServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function getSearchCustomerTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  			=> "required",
				"customer_id"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->getSearchCustomerTransactionServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function getSearchTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  			=> "required",
				"date"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->getSearchTransactionServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function getSearchRangeTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  			=> "required",
				"date_from"		  => "required",
				"date_to"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->getSearchRangeTransactionServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function newTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			    => "required",
			"token"		  			    => "required",
			"transaction_title" 		=> "required",
            "transaction_price" 	    => "required",
            "transaction_items" 	    => "required",
            "transaction_discount" 	    => "required",
			"transaction_total" 	    => "required",
			"customer_id"		 	    => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->newTransactionServiceFunction($params);
			if(!$resultFromService){
				return ['responseCode' => 400,'error' => true, 'errorMsg' => "Not enough stocks"];
			}else{
				return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
			}
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function updateTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"transaction_id"	  	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->updateTransactionServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
    }
    
    public function deleteTransactionModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"transaction_id"	  	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->transactionService->deleteTransactionServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}
}
