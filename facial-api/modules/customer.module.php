<?php
namespace modules;
class CustomerModule{
	private $customerService = null;
	public function __construct(){
		$this->customerService = new \services\CustomerService();
	}

	public function getCustomerModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->customerService->getCustomersServiceFunction();
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
    }

    public function getOneCustomersModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
				"account_id"		  => "required",
				"token"		  => "required",
				"customer_id"		  => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->customerService->getOneCustomersServiceFunction($params);
			return ['responseCode' => 200, 'status' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400,'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function newCustomerModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			    => "required",
			"token"		  			    => "required",
			"customer_name" 		    => "required",
            "customer_address"	        => "required",
            "customer_birthday"	        => "required",
            "customer_contact"	        => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->customerService->newCustomerServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}

	public function updateCustomerModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"customer_id"	  	    => "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->customerService->updateCustomerServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
    }
    
    public function deleteCustomerModuleFunction($params){
		$isDataValid = \Gump::is_valid($params, [
			"account_id" 			=> "required",
			"token"		  			=> "required",
			"customer_id"	    	=> "required",
		]);

		if($isDataValid === true){
			$resultFromService = $this->customerService->deleteCustomerServiceFunction($params);
			return ['responseCode' => 200, 'requestStatus' => 'success', 'data' => $resultFromService];
		}else{
			return ['responseCode' => 400, 'error' => true, 'errorMsg' => $isDataValid];
		}
	}
}
