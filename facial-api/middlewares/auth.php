<?php
namespace middlewares;
use tools\ToolService;
class auth
{
    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
           /* GET TOKEN */
                    if ($request->hasHeader('token')) {
                        $token = $request->getHeader('token')[0];
                    } else if (isset($_COOKIE['token'])) {
                        $token = $_COOKIE['token'];
                    } else {
                        $token = false;
                    }
                    // $token = "eyJhY2NvdW50X2lkIjoiMSIsInRva2VuIjoiMDAwYjEyN2I4ZGNmNDViNWY0MjgzZTVkMjFhY2M0YjIifXNwZWNpYWwga2V5";

                    if($token == false){
                        return $response->withJson( ['message' => 'unauthorized','status' => 401 ] )->withStatus(401);

                    }else{

                        $token = ToolService::encrypt_decrypt('decrypt', $token);
                        $request  = $request->withAttribute('token', $token['token']);
                        $request  = $request->withAttribute('account_id', $token['account_id']);

                        // return $response->withJson( $token )->withStatus(401);

                        return $next($request,$response);

                    }
                    // print_r($token);

    }
}