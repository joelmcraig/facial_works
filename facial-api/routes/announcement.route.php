<?php

use middlewares\auth;

/* Sample for Test Route API GET - The get route passing parameter will be on the url, it will be accessible in the $args - proper format for get url param is route_name/{param_name} */
$app->get('/announcementList', function($request, $response, $args){
    $module = new modules\AnnouncementModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    
    $result    = $module->getAnnouncementModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->post('/newAnnouncement', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\AnnouncementModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->newAnnouncementModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/updateAnnouncement', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\AnnouncementModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->updateAnnouncementModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/deleteAnnouncement', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\AnnouncementModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->deleteAnnouncementModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );



?>
