<?php

use middlewares\auth;

/* Sample for Test Route API GET - The get route passing parameter will be on the url, it will be accessible in the $args - proper format for get url param is route_name/{param_name} */
$app->get('/transactionList', function($request, $response, $args){
    $module = new modules\TransactionModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    
    $result    = $module->getTransactionModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->get('/transactionList/{transaction_id}', function($request, $response, $args){
    $module = new modules\TransactionModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    $params['transaction_id'] = $args['transaction_id'];
    
    $result    = $module->getOneTransactionModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->get('/transactionSearchCustomer/{customer_id}', function($request, $response, $args){
    $module = new modules\TransactionModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    $params['customer_id'] = $args['customer_id'];
    
    $result    = $module->getSearchCustomerTransactionModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->get('/transactionSearch/{date}', function($request, $response, $args){
    $module = new modules\TransactionModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    $params['date'] = $args['date'];
    
    $result    = $module->getSearchTransactionModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->get('/transactionSearchRange/{date_from}/{date_to}', function($request, $response, $args){
    $module = new modules\TransactionModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    $params['date_from'] = $args['date_from'];
    $params['date_to'] = $args['date_to'];
    
    $result    = $module->getSearchRangeTransactionModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->post('/newTransaction', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\TransactionModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->newTransactionModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/updateTransaction', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\TransactionModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->updateTransactionModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/deleteTransaction', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\TransactionModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->deleteTransactionModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );



?>
