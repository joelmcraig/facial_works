<?php

use middlewares\auth;

/* Sample for Test Route API GET - The get route passing parameter will be on the url, it will be accessible in the $args - proper format for get url param is route_name/{param_name} */
$app->get('/serviceList', function($request, $response, $args){
    $module = new modules\ServiceModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    
    $result    = $module->getServicesModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->get('/serviceList/{service_id}', function($request, $response, $args){
    $module = new modules\ServiceModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    $params['service_id'] = $args['service_id'];
    
    $result    = $module->getOneServicesModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->post('/newService', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\ServiceModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->newServicesModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/updateService', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\ServiceModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->updateServicesModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );



?>
