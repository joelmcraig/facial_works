<?php
/* Sample for Test Route API GET - The get route passing parameter will be on the url, it will be accessible in the $args - proper format for get url param is route_name/{param_name} */
$app->get('/getTest/{id}/{name}', function ($request, $response, $args) {
	$module = new modules\bookingModule();
	$variable1 = $args['id'];
	$variable2 = $args['name'];
	$params    = ["id" => $variable1, "name" => $variable2];
    $result    = $module->testModuleFunction1($params);
    return $response->withjson($result, $result['resposeCode']);
    
});

/* Sample for Test Route API POST - The post route passing parameter will be the body via x-www-form-urlencoded or formdata (binary files eg. Images / files), it will be accessible in the $request->getParsedBody() function with array data type */
$app->post('/login', function($request, $response, $args){
	$params = $request->getParsedBody();
	$module = new modules\LoginModule();
	$result = $module->loginUserFunction($params);
	return $response->withjson($result, $result['responseCode']);
});

$app->post('/postLogin', function($request, $response, $args){
	$params = $request->getParsedBody();
	$module = new modules\bookingModule();
	$result = $module->testModuleFunction2($params);
	return $response->withjson($result, $result['resposeCode']);
});