<?php

use middlewares\auth;

/* Sample for Test Route API GET - The get route passing parameter will be on the url, it will be accessible in the $args - proper format for get url param is route_name/{param_name} */
$app->get('/productList', function($request, $response, $args){
    $module = new modules\ProductModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    
    $result    = $module->getProductsModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->get('/productList/{product_id}', function($request, $response, $args){
    $module = new modules\ProductModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    $params['product_id'] = $args['product_id'];
    
    $result    = $module->getOneProductsModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->post('/newProduct', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\ProductModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->newProductsModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/updateProduct', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\ProductModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->updateProductsModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );



?>
