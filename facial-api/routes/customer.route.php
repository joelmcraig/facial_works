<?php

use middlewares\auth;

/* Sample for Test Route API GET - The get route passing parameter will be on the url, it will be accessible in the $args - proper format for get url param is route_name/{param_name} */
$app->get('/customerList', function($request, $response, $args){
    $module = new modules\CustomerModule();

    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    
    $result    = $module->getCustomerModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->get('/customerList/{customer_id}', function($request, $response, $args){
    $module = new modules\CustomerModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');
    $params['customer_id'] = $args['customer_id'];
    
    $result    = $module->getOneCustomersModuleFunction($params);
    return $response->withjson($result, $result['responseCode']);
    
})->add( new auth() );

$app->post('/newCustomer', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\CustomerModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->newCustomerModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/updateCustomer', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\CustomerModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->updateCustomerModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );

$app->post('/deleteCustomer', function($request, $response, $args){
	$params = $request->getParsedBody();
    $module = new modules\CustomerModule();
    
    $params['account_id'] = $request->getAttribute('account_id');
    $params['token'] = $request->getAttribute('token');

    // print_r($params);exit;

	$result = $module->deleteCustomerModuleFunction($params);
	return $response->withjson($result, $result['responseCode']);
})->add( new auth() );



?>
