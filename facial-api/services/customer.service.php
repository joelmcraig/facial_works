<?php
namespace services;
use PDO;

class CustomerService{
	private $dbConfig = null;
	private $databaseOpenConnection = null;
	public function __construct(){
		$this->dbConfig = new \configs\databaseConfigs();
		$this->databaseOpenConnection = new \medoo($this->dbConfig->mysqlConfig());
	}

	public function getCustomersServiceFunction(){

		$data = $this->databaseOpenConnection->query("SELECT * FROM customers ORDER BY customer_name")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }

    public function getOneCustomersServiceFunction($params){
        $customer_id = $params['customer_id'];
		$data = $this->databaseOpenConnection->query("SELECT * FROM customers WHERE customer_id = $customer_id")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }
    
    public function newCustomerServiceFunction($params){
        // print_r($params);exit;

        $check = $this->databaseOpenConnection->query("SELECT * FROM customers WHERE customer_name = '".$params['customer_name']."' OR customer_contact = '".$params['customer_contact']."'")->fetchAll(PDO::FETCH_ASSOC);
        if($check){
            // print_r($check);exit;
            $action = $this->databaseOpenConnection->log();

            $action = str_replace(['"',"'"], ["",""],$action[0]);

            if($check[0]['is_deleted'] == 1){
                $sql_update = $this->databaseOpenConnection->update("customers",[
                    "is_deleted" 		    => 0,
                ],["customer_id"             => $check[0]['customer_id']]);

                $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
                    "action" 		=> $action,
                    "message"       => "Account ID: ".$params['account_id']." restores the data of our customer named: ".$check[0]['customer_name'].".",
                    "date_created"	=> date("Y-m-d H:i:s")
                ]);

                $data["inserted"] = true;
            }else{

                $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
                    "action" 		=> $action,
                    "message"       => "Account ID: ".$params['account_id']." tried to add new customer but customer already existed.",
                    "date_created"	=> date("Y-m-d H:i:s")
                ]);

                $data = array();
                $data['inserted'] = false;
                $data['message'] = "Customer already exist.";
            }
        }else{
            $sql_insert = $this->databaseOpenConnection->insert("customers",[
                "customer_name" 		    => $params['customer_name'],
                "customer_address"          => $params['customer_address'],
                "customer_birthday"         => $params['customer_birthday'],
                "customer_contact"          => $params['customer_contact'],
                "date_created"	            => date("Y-m-d H:i:s")
            ]);

            $action = $this->databaseOpenConnection->log();

            $action = str_replace(['"',"'"], ["",""],$action[0]);

            // print_r($action);exit;

            $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
                "action" 		=> $action,
                "message"       => "Account ID: ".$params['account_id']." added new customer.",
                "date_created"	=> date("Y-m-d H:i:s")
            ]);

            $data["inserted"] = true;
        }

		return $data;
    }
    
    public function updateCustomerServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->update("customers",[
            "customer_name" 		    => $params['customer_name'],
            "customer_address"          => $params['customer_address'],
            "customer_birthday"          => $params['customer_birthday'],
            "customer_contact"          => $params['customer_contact'],
        ],["customer_id"             => $params['customer_id']]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." updates the customers information of ".$params['customer_name'].".",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
    }
    
    public function deleteCustomerServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->update("customers",[
            "is_deleted" 		    => 1,
        ],["customer_id"             => $params['customer_id']]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." deletes a customer.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
	}
}
