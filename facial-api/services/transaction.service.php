<?php
namespace services;
use PDO;

class TransactionService{
	private $dbConfig = null;
	private $databaseOpenConnection = null;
	public function __construct(){
		$this->dbConfig = new \configs\databaseConfigs();
		$this->databaseOpenConnection = new \medoo($this->dbConfig->mysqlConfig());
	}

	public function getTransactionServiceFunction(){

		$data = $this->databaseOpenConnection->query("SELECT *, DATE_FORMAT(date_created,'%b %d, %Y') as date_created FROM transactions ORDER BY date_created DESC")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }

    public function getOneTransactionServiceFunction($params){
        $transaction_id = $params['transaction_id'];
		$data = $this->databaseOpenConnection->query("SELECT * FROM transactions WHERE transaction_id = $transaction_id")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }

    public function getSearchCustomerTransactionServiceFunction($params){
        $customer_id = $params['customer_id'];
		$data = $this->databaseOpenConnection->query("SELECT * FROM transactions WHERE customer_id = $customer_id")->fetchAll(PDO::FETCH_ASSOC);
		return $data;
    }

    public function getSearchTransactionServiceFunction($params){
        $date = $params['date'];
		$data = $this->databaseOpenConnection->query("SELECT * FROM transactions WHERE DATE(date_created) = '$date' AND transaction_type = 'sold'")->fetchAll(PDO::FETCH_ASSOC);
		return $data;
    }

    public function getSearchRangeTransactionServiceFunction($params){
        $date_from = $params['date_from'];
        $date_to = $params['date_to'];
		$data = $this->databaseOpenConnection->query("SELECT * FROM transactions WHERE (date_created BETWEEN  '$date_from' AND '$date_to') AND transaction_type = 'sold'")->fetchAll(PDO::FETCH_ASSOC);
		return $data;
    }
    
    public function newTransactionServiceFunction($params){
        // print_r($params);exit;
        $tableCon = null;
        $title = $params['transaction_title'];
        $items = $params['transaction_items'];
        if($params['transaction_type'] == "Product"){
            $has_stocks = $this->databaseOpenConnection->query("SELECT * FROM products WHERE product_title = '$title' AND product_stocks >= $items")->fetchAll(PDO::FETCH_ASSOC);
            if(!$has_stocks){
                return false;
            }else{
                $sql_update = $this->databaseOpenConnection->update("products",[
                    "product_stocks[-]" 		    => $items,
                ],["product_title"             => $params['transaction_title']]);
        
                $action = $this->databaseOpenConnection->log();
        
                $action = str_replace(['"',"'"], ["",""],$action[0]);
        
                // print_r($action);exit;
        
                $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
                    "action" 		=> $action,
                    "message"       => "Account ID: ".$params['account_id']." updates the transaction.",
                    "date_created"	=> date("Y-m-d H:i:s")
                ]);

            }
        }

		$sql_insert = $this->databaseOpenConnection->insert("transactions",[
            "transaction_title" 		    => $params['transaction_title'],
            "transaction_unit"              => $params['transaction_unit'],
            "transaction_price"             => $params['transaction_price'],
            "transaction_items"             => $params['transaction_items'],
            "transaction_discount"          => $params['transaction_discount'],
            "transaction_total"             => $params['transaction_total'],
            "transaction_type"              => "sold",
            "date_created"	                => date("Y-m-d H:i:s"),
            "account_id"                    => $params['account_id'],
            "customer_id"                    => $params['customer_id'],
        ]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." added new transaction.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["inserted"] = true;

		return $data;
    }
    
    public function updateTransactionServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->update("transactions",[
            "transaction_title" 		    => $params['transaction_title'],
            "transaction_unit"              => $params['transaction_unit'],
            "transaction_price"             => $params['transaction_price'],
            "transaction_items"             => $params['transaction_items'],
            "transaction_discount"          => $params['transaction_discount'],
            "transaction_total"             => $params['transaction_total'],
            "date_created"	                => date("Y-m-d H:i:s"),
            "account_id"                    => $params['account_id'],
        ],["transaction_id"             => $params['transaction_id']]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." updates the transaction.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
    }
    
    public function deleteTransactionServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->delete("transactions",[
            "transaction_id"   => $params['transaction_id'],
        ]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." deletes the announcement.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
	}
}
