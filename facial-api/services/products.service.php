<?php
namespace services;
use PDO;

class ProductsService{
	private $dbConfig = null;
	private $databaseOpenConnection = null;
	public function __construct(){
		$this->dbConfig = new \configs\databaseConfigs();
		$this->databaseOpenConnection = new \medoo($this->dbConfig->mysqlConfig());
	}

	public function getProductsServiceFunction(){

		$data = $this->databaseOpenConnection->query("SELECT * FROM products")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }

    public function getOneProductsServiceFunction($params){
        $product_id = $params['product_id'];
		$data = $this->databaseOpenConnection->query("SELECT * FROM products WHERE product_id = $product_id")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }
    
    public function newProductsServiceFunction($params){
        // print_r($params);exit;
        $product_title = $params['product_title'];
        $hasProducts = $this->databaseOpenConnection->query("SELECT * FROM products WHERE product_title = '$product_title'")->fetchAll(PDO::FETCH_ASSOC);
        if($hasProducts){

            $sql_update = $this->databaseOpenConnection->update("products",[
                "product_stocks[+]" 		    => $params['product_stocks'],
            ],["product_title"             => $params['product_title']]);
    
            $action = $this->databaseOpenConnection->log();
    
            $action = str_replace(['"',"'"], ["",""],$action[0]);
    
            // print_r($action);exit;

            $sql_insert_transaction = $this->databaseOpenConnection->insert("transactions",[
                "transaction_title" 		    => $params['product_title'],
                "transaction_unit"              => $params['product_unit'],
                "transaction_price"             => $params['product_price'],
                "transaction_items"             => $params['product_stocks'],
                "transaction_type"              => "add product",
                "date_created"	                => date("Y-m-d H:i:s"),
                "account_id"                    => $params['account_id'],
            ]);
    
            $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
                "action" 		=> $action,
                "message"       => "Account ID: ".$params['account_id']." add new stocks.",
                "date_created"	=> date("Y-m-d H:i:s")
            ]);

            $data["updated"] = true;
            
        }else{

            $sql_insert = $this->databaseOpenConnection->insert("products",[
                "product_title" 		=> $params['product_title'],
                "product_description"   => $params['product_description'],
                "product_unit"          => $params['product_unit'],
                "product_price"         => $params['product_price'],
                "product_stocks"        => $params['product_stocks'],
                "date_created"	        => date("Y-m-d H:i:s")
            ]);

            $sql_insert_transaction = $this->databaseOpenConnection->insert("transactions",[
                "transaction_title" 		    => $params['product_title'],
                "transaction_unit"              => $params['product_unit'],
                "transaction_price"             => $params['product_price'],
                "transaction_items"             => $params['product_stocks'],
                "transaction_type"              => "new entry",
                "date_created"	                => date("Y-m-d H:i:s"),
                "account_id"                    => $params['account_id'],
            ]);

            $action = $this->databaseOpenConnection->log();

            $action = str_replace(['"',"'"], ["",""],$action[0]);

            // print_r($action);exit;

            $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
                "action" 		=> $action,
                "message"       => "Account ID: ".$params['account_id']." added new product.",
                "date_created"	=> date("Y-m-d H:i:s")
            ]);

            $data["inserted"] = true;
        }

        

		return $data;
    }
    
    public function updateProductsServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->update("products",[
            "product_title" 		=> $params['product_title'],
            "product_description"   => $params['product_description'],
            "product_unit"          => $params['product_unit'],
            "product_price"         => $params['product_price'],
            "product_stocks"        => $params['product_stocks'],
        ],["product_id"             => $params['product_id']]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." updates the product.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
	}
}
