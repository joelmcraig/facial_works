<?php
namespace services;
use PDO;

class AnnouncementService{
	private $dbConfig = null;
	private $databaseOpenConnection = null;
	public function __construct(){
		$this->dbConfig = new \configs\databaseConfigs();
		$this->databaseOpenConnection = new \medoo($this->dbConfig->mysqlConfig());
	}

	public function getAnnouncementServiceFunction(){

		$data = $this->databaseOpenConnection->query("SELECT * FROM announcements ORDER BY date_created DESC LIMIT 3")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }
    
    public function newAnnouncementServiceFunction($params){
        // print_r($params);exit;

		$sql_insert = $this->databaseOpenConnection->insert("announcements",[
            "announcement_title" 		    => $params['announcement_title'],
            "announcement_description"      => $params['announcement_description'],
            "announcement_image"            => $params['announcement_image'],
            "account_id"                    => $params['account_id'],
            "date_created"	                => date("Y-m-d H:i:s")
        ]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." added new announcement.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["inserted"] = true;

		return $data;
    }
    
    public function updateAnnouncementServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->update("announcements",[
            "announcement_title" 		=> $params['announcement_title'],
            "announcement_description"   => $params['announcement_description'],
            "announcement_image"          => $params['announcement_image'],
        ],["announcement_id"             => $params['announcement_id']]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." updates the announcement.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
    }
    
    public function deleteAnnouncementServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->delete("announcements",[
            "announcement_id"   => $params['announcement_id'],
        ]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." deletes the announcement.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
	}
}
