<?php
namespace services;
use PDO;

class ServiceService{
	private $dbConfig = null;
	private $databaseOpenConnection = null;
	public function __construct(){
		$this->dbConfig = new \configs\databaseConfigs();
		$this->databaseOpenConnection = new \medoo($this->dbConfig->mysqlConfig());
	}

	public function getServicesServiceFunction(){

		$data = $this->databaseOpenConnection->query("SELECT * FROM services")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }

    public function getOneServicesServiceFunction($params){
        $service_id = $params['service_id'];
		$data = $this->databaseOpenConnection->query("SELECT * FROM services WHERE service_id = $service_id")->fetchAll(PDO::FETCH_ASSOC);
		
		return $data;
    }
    
    public function newServicesServiceFunction($params){
        // print_r($params);exit;

		$sql_insert = $this->databaseOpenConnection->insert("services",[
            "service_title" 		=> $params['service_title'],
            "service_description"   => $params['service_description'],
            "service_unit"          => $params['service_unit'],
            "service_price"         => $params['service_price'],
            "date_created"	        => date("Y-m-d H:i:s")
        ]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        $sql_insert_transaction = $this->databaseOpenConnection->insert("transactions",[
            "transaction_title" 		    => $params['service_title'],
            "transaction_unit"              => $params['service_unit'],
            "transaction_price"             => $params['service_price'],
            "transaction_type"              => "add service",
            "date_created"	                => date("Y-m-d H:i:s"),
            "account_id"                    => $params['account_id'],
        ]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." added new service.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["inserted"] = true;

		return $data;
    }
    
    public function updateServicesServiceFunction($params){
        // print_r($params);exit;

		$sql_update = $this->databaseOpenConnection->update("services",[
            "service_title" 		=> $params['service_title'],
            "service_description"   => $params['service_description'],
            "service_unit"          => $params['service_unit'],
            "service_price"         => $params['service_price'],
        ],["service_id"             => $params['service_id']]);

        $action = $this->databaseOpenConnection->log();

        $action = str_replace(['"',"'"], ["",""],$action[0]);

        $sql_insert_transaction = $this->databaseOpenConnection->insert("transactions",[
            "transaction_title" 		    => $params['service_title'],
            "transaction_unit"              => $params['service_unit'],
            "transaction_price"             => $params['service_price'],
            "transaction_type"              => "new service",
            "date_created"	                => date("Y-m-d H:i:s"),
            "account_id"                    => $params['account_id'],
        ]);

        // print_r($action);exit;

        $sql_insert_log = $this->databaseOpenConnection->insert("logs",[
            "action" 		=> $action,
            "message"       => "Account ID: ".$params['account_id']." updates the service.",
            "date_created"	=> date("Y-m-d H:i:s")
        ]);

        $data["updated"] = true;

		return $data;
	}
}
