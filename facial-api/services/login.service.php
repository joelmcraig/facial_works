<?php
namespace services;
use PDO;
use tools\ToolService;

class LoginService{
	private $dbConfig = null;
	private $databaseOpenConnection = null;
	private $tool = null;
	public function __construct(){
		$this->dbConfig = new \configs\databaseConfigs();
		$this->databaseOpenConnection = new \medoo($this->dbConfig->mysqlConfig());
		$this->tool = new ToolService();
	}

	public function loginServiceFunction($params){
		$username = $params['username'];
		$password = $params['password'];
		$loggedin = $this->databaseOpenConnection->query("SELECT * FROM accounts WHERE username = '$username' AND password = '$password'")->fetchAll(PDO::FETCH_ASSOC);

		if($loggedin){
			$data['loggedin'] = true;
			$data['name'] = $loggedin[0]['name'];
			$data['user_level'] = $loggedin[0]['user_level'];
			$data['token'] =  $this->tool->generateToken();
			$data['account_id'] = $loggedin[0]['account_id'];

			$auth = json_encode([
				'account_id'	=>	$loggedin[0]['account_id'],
				'token'			=>	$this->tool->generateToken(),
			]);
			$data['auth_token'] = $this->tool->encrypt_decrypt('encrypt', $auth);
			
		}else{
			$data['loggedin'] = false;
		}
		
		return $data;
	}
}
