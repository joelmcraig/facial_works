<?php
/* Initialized/declare libraries from composer installation */
require 'vendor/autoload.php';
$config = [
    'settings' => [
        'displayErrorDetails' => 1,
    ]
];

/* Initialized */
$app = new \Slim\App($config);

foreach(glob("services/*.service.php") as $file){
    require $file;
}

foreach(glob("middlewares/*.php") as $file){
    require $file;
}

foreach(glob("modules/*.module.php") as $file){
    require $file;
}

foreach(glob("routes/*.route.php") as $file){
    require $file;
}

foreach(glob("configs/*.config.php") as $file){
    require $file;
}

$app->run();