$(document).ready(function(){
    var baseApi = "http://facialworks.net/facial-api/index.php/";
    // var baseApi = "http://localhost/facial_works/facial-api/index.php/";

    // Events
	$('#login').click(function(){
			
		var username = $('#nameUser').val();
        var password = $('#passUser').val();
        
        $.ajax({
            type: "POST",
            url: baseApi + "login",
            data: {'username':username,
                    'password':password
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        // alert(data["data"].auth_token);
                        Materialize.toast('Login Success.', 4000);
                        $.cookie("token", data["data"].auth_token);
                        $.cookie("name", data["data"].name);
                        $.cookie("user_level", data["data"].user_level);
                        window.location.href = "index.html";
                        // $.each (data["data"], function (val) {
                        //     console.log (val);
                        // });
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Account does not exist.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
		
    });

    $('.signout').click(function(){
			
        $.removeCookie("token");
        window.location.href = "login.html";
		
    });
    

    $('#transaction-content').change(function(){
        var transaction_type = $('#transaction-content option:selected').text();
        // alert(transaction_type);
        // $('#title-content').append($('option',{
        //     value: 1,
        //     text: "qwewqeqweqw",
        // }));
        // initialize
        $("#title-content").html('None');
        $('#title-content').material_select();
        $('#title-content').on('contentChanged', function() {
            $(this).material_select();
          });

        if(transaction_type == "Product"){
    
            // var newValue = "hello worldo";
            // var $newOpt = $("<option>").attr("value",newValue).text(newValue)
            // $('#title-content').append($newOpt);
            $.ajax({
                type: "GET",
                beforeSend: function(request) {
                    request.setRequestHeader("token", $.cookie("token"));
                },
                url: baseApi + "productList",
                dataType: "json",
                success: function(data){
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        var $newOpt = $("<option>").attr("value",datas[i].product_price + "," + datas[i].product_unit).text(datas[i].product_title);
                        $('#title-content').append($newOpt);
                        
                    });
                    $("#title-content").trigger('contentChanged');
                    var price_unit = $('#title-content option:selected').val();
       
                    var result = price_unit.split(',');
                    // alert(result[0]);
                    $('#price').val(result[0]);
                    $('#unit').val(result[1]);
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Failed to load services.', 4000);
                }
            });
        }else if(transaction_type == "Service"){

    
            // var newValue = "echoser";
            // var $newOpt = $("<option>").attr("value",newValue).text(newValue)
            // $('#title-content').append($newOpt);
            $.ajax({
                type: "GET",
                beforeSend: function(request) {
                    request.setRequestHeader("token", $.cookie("token"));
                },
                url: baseApi + "serviceList",
                dataType: "json",
                success: function(data){
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        var $newOpt = $("<option>").attr("value",datas[i].service_price + "," + datas[i].service_unit).text(datas[i].service_title);
                        $('#title-content').append($newOpt);
                        
                    });
                    $("#title-content").trigger('contentChanged');
                    var price_unit = $('#title-content option:selected').val();
       
                    var result = price_unit.split(',');
                    // alert(result[0]);
                    $('#price').val(result[0]);
                    $('#unit').val(result[1]);
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Failed to load services.', 4000);
                }
            });
        }
    });

    $('#title-content').change(function(){
        $("#title-content").trigger('contentChanged');
        var price_unit = $('#title-content option:selected').val();
       
        var result = price_unit.split(',');
        // alert(result[0]);
        $('#price').val(result[0]);
        $('#unit').val(result[1]);
    });

    //Add Transaction
    $('#submit').click(function(){
        
        var customer_id = $('#customer-list option:selected').val();
        var transaction_type = $('#transaction-content option:selected').text();
        var title   = $('#title-content option:selected').text();
        var unit    = $('#unit').val();
        var price   = $('#price').val();
        var items   = $('#items').val();
        var discount   = $('#discount').val();
        var total = (price * items) + ((price * items)*(discount/100));
        // alert((price*items) + " + " + ((price * items)*(discount/100)) + " = " + total);
        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "newTransaction",
            data: {'transaction_type':transaction_type,
                    'transaction_title':title,
                    'transaction_price':price,
                    'transaction_unit':unit,
                    'transaction_items':items,
                    'transaction_discount':discount,
                    'transaction_total':total,
                    'customer_id':customer_id
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Transaction Added.', 4000);
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Transaction Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Add Products
    $('#submitNewCustomer').click(function(){
        
        var customer_name   = $('#new_customer_name').val();
        var customer_address    = $('#new_customer_address').val();
        var customer_birthday   = $('#new_customer_birthdate').val();
        var customer_contact   = $('#new_customer_contact').val();

        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
                // request.setRequestHeader("Cache-Control", "no-cache, no-store, must-revalidate");
                // request.setRequestHeader("Pragma", no-cache);
                // request.setRequestHeader("Expires", 0);
            },
            url: baseApi + "newCustomer",
            data: {'customer_name':customer_name,
                    'customer_address':customer_address,
                    'customer_birthday':customer_birthday,
                    'customer_contact':customer_contact
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Customer Added.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Customer Adding Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Add Products
    $('#submitNewProduct').click(function(){
        
        var product_title   = $('#new_product_title').val();
        var product_description    = $('#new_product_description').val();
        var product_unit   = $('#new_product_unit').val();
        var product_price   = $('#new_product_price').val();
        var product_stocks   = $('#new_product_stocks').val();

        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "newProduct",
            data: {'product_title':product_title,
                    'product_description':product_description,
                    'product_unit':product_unit,
                    'product_price':product_price,
                    'product_stocks':product_stocks
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Products Added.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Product Adding Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Add Services
    $('#submitNewService').click(function(){
        
        var service_title   = $('#new_service_title').val();
        var service_description    = $('#new_service_description').val();
        var service_unit   = $('#new_service_unit').val();
        var service_price   = $('#new_service_price').val();

        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "newService",
            data: {'service_title':service_title,
                    'service_description':service_description,
                    'service_unit':service_unit,
                    'service_price':service_price
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Service Added.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Service Adding Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Delegate on Announcement
    $(document).on("click","#announcement-content li button.btn", function() { // any button
        // console.log($(this).val());
        // alert($(this).val());
        var announcement_id = $(this).val();

        if(confirm("Are you sure you want to delete this announcement?")){
            // alert("delete id: " + announcement_id);
            $.ajax({
                type: "POST",
                beforeSend: function(request) {
                    request.setRequestHeader("token", $.cookie("token"));
                },
                url: baseApi + "deleteAnnouncement",
                data: {'announcement_id':announcement_id
                    },
                    dataType: "json",
                    success: function(data){
                        var data = data;
                        if(data["responseCode"] == 200){
                            Materialize.toast('Announcement Deleted.', 4000);
                            setTimeout(function(){// wait for 5 secs(2)
                                location.reload(); // then reload the page.(3)
                           }, 2000); 
                        }
                    },
                    error: function (data) {
                        var data = data;
                        Materialize.toast('Announcement Delete Failed.', 4000);
                        // console.log(result);
                        // alert(" Can't do because: ");
                    }
            });
        }
        else{
            return false;
        }
    });

    $('#submitAnnouncement').click(function(){
        
        var announcement_title          = $('#announcement_title').val();
        var announcement_description    = $('#announcement_description').val();

        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "newAnnouncement",
            data: {'announcement_title':announcement_title,
                    'announcement_description':announcement_description
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Announcement Added.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Announcement Add Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Delegate on Customers
    $(document).on("click","#customer-content tbody tr td button.btn", function() { // any button
        // console.log($(this).val());
        // alert($(this).val());
        var customer_id = $(this).val();
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "customerList/" + customer_id,
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    var datas = data["data"];
                    $.each (datas, function(i, item) {

                        $('#submitCustomer').val(datas[i].customer_id);
                        $('#customer_name').val(datas[i].customer_name);
                        $('#customer_address').val(datas[i].customer_address);
                        $('#customer_birthday').val(datas[i].customer_birthday);
                        $('#customer_contact').val(datas[i].customer_contact);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Fetch Failed.', 4000);
                // console.log(result);
                // alert(" Can't do because: ");
            }
        });
    });

    $('#submitCustomer').click(function(){
        
        var customer_id   = $('#submitCustomer').val();
        var customer_name    = $('#customer_name').val();
        var customer_address   = $('#customer_address').val();
        var customer_birthday   = $('#customer_birthday').val();
        var customer_contact   = $('#customer_contact').val();
        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "updateCustomer",
            data: {'customer_id':customer_id,
                    'customer_name':customer_name,
                    'customer_address':customer_address,
                    'customer_birthday':customer_birthday,
                    'customer_contact':customer_contact
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Customer Updated.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Customer Update Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Delegate on Products
    $(document).on("click","#product-content tbody tr td button.btn", function() { // any button
        // console.log($(this).val());
        // alert($(this).val());
        var product_id = $(this).val();
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "productList/" + product_id,
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].product_id);
                        // console.log(datas[i].product_title);
                        // console.log(datas[i].product_description);
                        // console.log(datas[i].product_unit);
                        // console.log(datas[i].product_price);
                        // console.log(datas[i].product_stocks);

                        $('#submitProduct').val(datas[i].product_id);
                        $('#product_title').val(datas[i].product_title);
                        $('#product_description').val(datas[i].product_description);
                        $('#product_unit').val(datas[i].product_unit);
                        $('#product_price').val(datas[i].product_price);
                        $('#product_stocks').val(datas[i].product_stocks);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Fetch Failed.', 4000);
                // console.log(result);
                // alert(" Can't do because: ");
            }
        });
    });

    $('#submitProduct').click(function(){
        
        var product_id   = $('#submitProduct').val();
        var product_title    = $('#product_title').val();
        var product_description   = $('#product_description').val();
        var product_unit   = $('#product_unit').val();
        var product_price   = $('#product_price').val();
        var product_stocks   = $('#product_stocks').val();
        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "updateProduct",
            data: {'product_id':product_id,
                    'product_title':product_title,
                    'product_description':product_description,
                    'product_unit':product_unit,
                    'product_price':product_price,
                    'product_stocks':product_stocks
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Products Updated.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Product Update Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Delegate on Services
    $(document).on("click","#service-content tbody tr td button.btn", function() { // any button
        // console.log($(this).val());
        // alert($(this).val());
        var service_id = $(this).val();
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "serviceList/" + service_id,
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        console.log(datas[i].service_id);
                        console.log(datas[i].service_title);
                        console.log(datas[i].service_description);
                        console.log(datas[i].service_unit);
                        console.log(datas[i].service_price);

                        $('#submitService').val(datas[i].service_id);
                        $('#service_title').val(datas[i].service_title);
                        $('#service_description').val(datas[i].service_description);
                        $('#service_unit').val(datas[i].service_unit);
                        $('#service_price').val(datas[i].service_price);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Fetch Failed.', 4000);
                // console.log(result);
                // alert(" Can't do because: ");
            }
        });
    });
    
    $('#submitService').click(function(){
        
        var service_id   = $('#submitService').val();
        var service_title    = $('#service_title').val();
        var service_description   = $('#service_description').val();
        var service_unit   = $('#service_unit').val();
        var service_price   = $('#service_price').val();

        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "updateService",
            data: {'service_id':service_id,
                    'service_title':service_title,
                    'service_description':service_description,
                    'service_unit':service_unit,
                    'service_price':service_price,
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Services Updated.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Service Update Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    //Delegate on Transactions
    $(document).on("click","#transactions-content tbody tr td button.btn", function() { // any button
        // console.log($(this).val());
        // alert($(this).val());
        var transaction_id = $(this).val();
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "transactionList/" + transaction_id,
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        console.log(datas[i].transaction_id);
                        console.log(datas[i].transaction_title);
                        console.log(datas[i].transaction_description);
                        console.log(datas[i].transaction_unit);
                        console.log(datas[i].transaction_price);

                        $('#submitTransaction').val(datas[i].transaction_id);
                        $('#transaction_title').val(datas[i].transaction_title);
                        $('#transaction_unit').val(datas[i].transaction_unit);
                        $('#transaction_price').val(datas[i].transaction_price);
                        $('#transaction_items').val(datas[i].transaction_items);
                        $('#transaction_discount').val(datas[i].transaction_discounts);
                        $('#transaction_total').val(datas[i].transaction_total);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Fetch Failed.', 4000);
                // console.log(result);
                // alert(" Can't do because: ");
            }
        });
    });
    
    $('#submitTransaction').click(function(){
        
        var transaction_id   = $('#submitTransaction').val();
        var transaction_title    = $('#transaction_title').val();
        var transaction_unit   = $('#transaction_unit').val();
        var transaction_price   = $('#transaction_price').val();
        var transaction_items   = $('#transaction_items').val();
        var transaction_discount   = $('#transaction_discount').val();
        var transaction_total   = $('#transaction_total').val();

        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "updateTransaction",
            data: {'transaction_id':transaction_id,
                    'transaction_title':transaction_title,
                    'transaction_unit':transaction_unit,
                    'transaction_price':transaction_price,
                    'transaction_items':transaction_items,
                    'transaction_discount':transaction_discount,
                    'transaction_total':transaction_total,
                },
                dataType: "json",
                success: function(data){
                    var data = data;
                    if(data["responseCode"] == 200){
                        Materialize.toast('Transactions Updated.', 4000);
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                       }, 2000); 
                    }
                },
                error: function (data) {
                    var data = data;
                    Materialize.toast('Transaction Update Failed.', 4000);
                    // console.log(result);
                    // alert(" Can't do because: ");
                }
        });
    });

    $('#searchCustomer').click(function(){
		
        var customer_id = $('#customer-search-list option:selected').val();

        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "transactionSearchCustomer/" + customer_id,
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    $("#transactions-content tbody").empty();
                    var datas = data["data"];
                    var totalSales = 0;
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].announcement_id);
                        // console.log(datas[i].announcement_title);
                        // console.log(datas[i].announcement_description);
                        // console.log(datas[i].announcement_image);
                        // console.log(datas[i].account_id);
                        console.log(datas[i].transaction_id);
                        totalSales += parseInt(datas[i].transaction_total);
                        var newRowContent = '\
                        <tr>\
                            <td>' + datas[i].transaction_title + '</td>\
                            <td>' + datas[i].transaction_unit + '</td>\
                            <td>PHP ' + datas[i].transaction_price + '</td>\
                            <td>' + datas[i].transaction_items + '</td>\
                            <td>' + datas[i].transaction_discount + '%</td>\
                            <td>PHP ' + datas[i].transaction_total + '</td>\
                            <td>' + datas[i].date_created + '</td>\
                        </tr>';

                        /*
                        <td><button name="updateTransaction" data-target="modal1"\
                             class="btn waves-effect waves-light right modal-trigger"\
                             value="' + datas[i].transaction_id + '">\
                             <i class="material-icons">edit</i>\
                             </button></td>\
                             */
                        $("#transactions-content tbody").append(newRowContent);
                    });
                    var newTotalContent = '\
                    <tr>\
                        <td><b>Total Sales: PHP ' + totalSales + '</b></td>\
                    </tr>';
                    $("#transactions-content tbody").append(newTotalContent);
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to find sales.', 4000);
            }
        });
		
    });

    $('#searchSales').click(function(){
		
		var date = $("#dateSales").val();

        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "transactionSearch/" + date,
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    $("#transactions-content tbody").empty();
                    var datas = data["data"];
                    var totalSales = 0;
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].announcement_id);
                        // console.log(datas[i].announcement_title);
                        // console.log(datas[i].announcement_description);
                        // console.log(datas[i].announcement_image);
                        // console.log(datas[i].account_id);
                        console.log(datas[i].transaction_id);
                        totalSales += parseInt(datas[i].transaction_total);
                        var newRowContent = '\
                        <tr>\
                            <td>' + datas[i].transaction_title + '</td>\
                            <td>' + datas[i].transaction_unit + '</td>\
                            <td>PHP ' + datas[i].transaction_price + '</td>\
                            <td>' + datas[i].transaction_items + '</td>\
                            <td>' + datas[i].transaction_discount + '%</td>\
                            <td>PHP ' + datas[i].transaction_total + '</td>\
                            <td>' + datas[i].date_created + '</td>\
                        </tr>';

                        /*
                        <td><button name="updateTransaction" data-target="modal1"\
                             class="btn waves-effect waves-light right modal-trigger"\
                             value="' + datas[i].transaction_id + '">\
                             <i class="material-icons">edit</i>\
                             </button></td>\
                             */
                        $("#transactions-content tbody").append(newRowContent);
                    });
                    var newTotalContent = '\
                    <tr>\
                        <td><b>Total Sales: PHP ' + totalSales + '</b></td>\
                    </tr>';
                    $("#transactions-content tbody").append(newTotalContent);
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to find sales.', 4000);
            }
        });
		
    });
    
    $('#searchSalesRange').click(function(){
		
        var dateFrom = $("#dateSalesFrom").val();
        var dateTo = $("#dateSalesTo").val();

        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "transactionSearchRange/" + dateFrom + "/" + dateTo,
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    $("#transactions-content tbody").empty();
                    var datas = data["data"];
                    var totalSales = 0;
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].announcement_id);
                        // console.log(datas[i].announcement_title);
                        // console.log(datas[i].announcement_description);
                        // console.log(datas[i].announcement_image);
                        // console.log(datas[i].account_id);
                        console.log(datas[i].transaction_id);
                        totalSales += parseInt(datas[i].transaction_total);
                        var newRowContent = '\
                        <tr>\
                            <td>' + datas[i].transaction_title + '</td>\
                            <td>' + datas[i].transaction_unit + '</td>\
                            <td>PHP ' + datas[i].transaction_price + '</td>\
                            <td>' + datas[i].transaction_items + '</td>\
                            <td>' + datas[i].transaction_discount + '%</td>\
                            <td>PHP ' + datas[i].transaction_total + '</td>\
                            <td>' + datas[i].date_created + '</td>\
                            <td>' + datas[i].transaction_type + '</td>\
                        </tr>';

                        /*
                        <td><button name="updateTransaction" data-target="modal1"\
                             class="btn waves-effect waves-light right modal-trigger"\
                             value="' + datas[i].transaction_id + '">\
                             <i class="material-icons">edit</i>\
                             </button></td>\
                             */
                        $("#transactions-content tbody").append(newRowContent);
                    });
                    var newTotalContent = '\
                    <tr>\
                        <td><b>Total Sales: PHP ' + totalSales + '</b></td>\
                    </tr>';
                    $("#transactions-content tbody").append(newTotalContent);
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to find sales.', 4000);
            }
        });
		
	});

    
});