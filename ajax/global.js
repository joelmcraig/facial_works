$(document).ready(function(){
	
	
	$('#login').click(function(){
		
		alert(1);
		
		var nameUser = $('#nameUser').val();
		var passUser = $('#passUser').val();
		
		//alert("username: " + nameUser + " / " + "password: " + passUser);
		
			$.ajax({
				type: "POST",
				url: "../facial-api/login",
				data: {'username':nameUser,
						'password':passUser
					},
					dataType: "json",
					success: function(result){
					$('#log').html(result.item1);
					//alert(1);
					}
				});
		//end of ajax here
		
	});
	
	$('#userList').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/user-list.php" );
		
	});
	
	$('#userListCancel').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/user-listCancel.php" );
		
	});
	
	$('#reportsListOfPatients').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/reports-listOfPatients.php" );
		
	});
	
	$('#reportsListOfReserved').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/reports-listOfReserved.php" );
		
	});
	
	$('#reportsListOfPending').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/reports-listOfPending.php" );
		
	});
	
	$('#reportsListOfCancelled').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/reports-listOfCancelled.php" );
		
	});
	
	$('#servicesListOfSession').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/services-listOfSession.php" );
		
	});
	
	$('#servicesAddSession').click(function(){
		
		$('#modalAddService').openModal();
		
	});
	
	$('#utilityInstruction').click(function(){
		
		$('#modalInstruction').openModal();
		
	});
	
	$('#addSession').click(function(){
		
		var session_name = $('#session_name').val();
		var session_time = $('#session_time').val();
		var session_price = $('#session_price').val();
		
		var re = /[^0-9]/g;    
		if ( re.test(session_time) ||  re.test(session_price) ){  
			// found a non-numeric value, handling error
			Materialize.toast('Time and Price should be numeric.', 4000);
		}else{
			$.ajax({
				type: "POST",
				url: "contents/services-insert.php",
				data: {'session_name':session_name,
						'session_time':session_time,
						'session_price':session_price
						},
						dataType: "json",
						success: function(result){
						$('#session_name').val('');
						$('#session_time').val('');
						$('#session_price').val('');
						$('#log').empty();
						$('#log').html(result.item1);
						$('#modalAddService').closeModal();
						}
					});
			//end of ajax here
		}

	});
	
	$('#salesByServices').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/sales-byServices.php" );
		
	});
	
	$('#salesByDay').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/sales-byDay.php" );
		
	});
	
	$('#salesByMonth').click(function(){
		
		//alert("User List");
		$('#loader').empty();
		$( "#loader" ).load( "contents/sales-byMonth.php" );
		
	});

	
	
});