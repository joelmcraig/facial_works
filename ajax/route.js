$(document).ready(function(){
    $('.modal').modal();
    var baseApi = "http://facialworks.net/facial-api/index.php/";
    // var baseApi = "http://localhost/facial_works/facial-api/index.php/";
    var domainPath = "/";

    var path = window.location.pathname;
    if (path != domainPath + "login.html"){
        if ($.cookie("token") == '' || $.cookie("token") == null) {
            window.location.href = "login.html";
        }
    }

    $('#logName').text($.cookie("name"));
    $('#logLevel').text($.cookie("user_level"));
    console.log(path);
    console.log(domainPath + "index.html");
    // Routes
    if (path == domainPath + "index.html" || path == domainPath){
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "announcementList",
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].announcement_id);
                        // console.log(datas[i].announcement_title);
                        // console.log(datas[i].announcement_description);
                        // console.log(datas[i].announcement_image);
                        // console.log(datas[i].account_id);
                        // console.log(datas[i].date_created);
                        $('#announcement-content').append('' +
                            '<li class="collection-item">'+
                                '<button name="deleteAnnouncement" class="btn btn-flat waves-effect" value="' + datas[i].announcement_id + '">' +
                                    '<i class="material-icons left">delete_forever</i>Remove</button>' +
                                '<span class="task-cat cyan">' + datas[i].announcement_title + '</span>' +
                                '<label>' + datas[i].announcement_description +
                                    '<a href="#" class="secondary-content">' +
                                        '<span class="ultra-small">' + datas[i].date_created + '</span>' +
                                    '</a>' +
                                '</label>' +
                            '</li>' +
                            '');
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to load announcement.', 4000);
            }
        });

    }else if (path == domainPath + "customers.html"){
       
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "customerList",
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    var datas = data["data"];
                    console.log(datas);
                    $.each (datas, function(i, item) {
                        var newRowContent = '';
                        
                        newRowContent = '\
                        <tr>\
                            <td>' + datas[i].customer_name + '</td>\
                            <td>' + datas[i].customer_address + '</td>\
                            <td>' + datas[i].customer_birthday + '</td>\
                            <td>' + datas[i].customer_contact + '</td>\
                            <td><button name="updateCustomer" data-target="modal1"\
                            class="btn waves-effect waves-light right modal-trigger"\
                            value="' + datas[i].customer_id + '">\
                            <i class="material-icons">edit</i>\
                            </button></td>\
                        </tr>';
                        

                        $("#customer-content tbody").append(newRowContent);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to load customers.', 4000);
            }
        });

    }else if (path == domainPath + "products.html"){
       
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "productList",
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].announcement_id);
                        // console.log(datas[i].announcement_title);
                        // console.log(datas[i].announcement_description);
                        // console.log(datas[i].announcement_image);
                        // console.log(datas[i].account_id);
                        console.log(datas[i].product_id);
                        var newRowContent = '';
                        if($.cookie("user_level") && $.cookie("user_level") != "Administrator"){
                            newRowContent = '\
                            <tr>\
                                <td>' + datas[i].product_title + '</td>\
                                <td>' + datas[i].product_description + '</td>\
                                <td>' + datas[i].product_unit + '</td>\
                                <td>PHP ' + datas[i].product_price + '</td>\
                                <td>' + datas[i].product_stocks + '</td>\
                            </tr>';
                        }else{
                            newRowContent = '\
                            <tr>\
                                <td>' + datas[i].product_title + '</td>\
                                <td>' + datas[i].product_description + '</td>\
                                <td>' + datas[i].product_unit + '</td>\
                                <td>PHP ' + datas[i].product_price + '</td>\
                                <td>' + datas[i].product_stocks + '</td>\
                                <td><button name="updateProduct" data-target="modal1"\
                                class="btn waves-effect waves-light right modal-trigger"\
                                value="' + datas[i].product_id + '">\
                                <i class="material-icons">edit</i>\
                                </button></td>\
                            </tr>';
                        }

                        $("#product-content tbody").append(newRowContent);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to load products.', 4000);
            }
        });

    }else if (path == domainPath + "services.html"){
       
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "serviceList",
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].announcement_id);
                        // console.log(datas[i].announcement_title);
                        // console.log(datas[i].announcement_description);
                        // console.log(datas[i].announcement_image);
                        // console.log(datas[i].account_id);
                        console.log(datas[i].service_id);
                        var newRowContent = '';
                        if($.cookie("user_level") && $.cookie("user_level") != "Administrator"){
                            newRowContent = '\
                            <tr>\
                                <td>' + datas[i].service_title + '</td>\
                                <td>' + datas[i].service_description + '</td>\
                                <td>' + datas[i].service_unit + '</td>\
                                <td>PHP ' + datas[i].service_price + '</td>\
                            </tr>';
                        }else{
                            newRowContent = '\
                            <tr>\
                                <td>' + datas[i].service_title + '</td>\
                                <td>' + datas[i].service_description + '</td>\
                                <td>' + datas[i].service_unit + '</td>\
                                <td>PHP ' + datas[i].service_price + '</td>\
                                <td><button name="updateService" data-target="modal1"\
                                class="btn waves-effect waves-light right modal-trigger"\
                                value="' + datas[i].service_id + '">\
                                <i class="material-icons">edit</i>\
                                </button></td>\
                            </tr>';
                        }

                        $("#service-content tbody").append(newRowContent);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to load services.', 4000);
            }
        });

    }else if (path == domainPath + "transactions.html"){
       
        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "transactionList",
            dataType: "json",
            success: function(data){
                var data = data;
                if(data["responseCode"] == 200){
                    // Materialize.toast('Fetching Success.', 4000);
                    var datas = data["data"];
                    $.each (datas, function(i, item) {
                        // console.log(datas[i].announcement_id);
                        // console.log(datas[i].announcement_title);
                        // console.log(datas[i].announcement_description);
                        // console.log(datas[i].announcement_image);
                        // console.log(datas[i].account_id);
                        console.log(datas[i].transaction_id);

                        var discount, total;
                        if(datas[i].transaction_discount != null){
                            discount = datas[i].transaction_discount + "%";
                        }else{
                            discount = 0;
                        }

                        if(datas[i].transaction_total != null){
                            total = "PHP " + datas[i].transaction_total;
                        }else{
                            total = 0;
                        }
                        var newRowContent = '\
                        <tr>\
                            <td>' + datas[i].transaction_title + '</td>\
                            <td>' + datas[i].transaction_unit + '</td>\
                            <td>PHP ' + datas[i].transaction_price + '</td>\
                            <td>' + datas[i].transaction_items + '</td>\
                            <td>' + discount + '</td>\
                            <td>' + total + '</td>\
                            <td>' + datas[i].date_created + '</td>\
                            <td>' + datas[i].transaction_type + '</td>\
                        </tr>';

                        /*
                        <td><button name="updateTransaction" data-target="modal1"\
                             class="btn waves-effect waves-light right modal-trigger"\
                             value="' + datas[i].transaction_id + '">\
                             <i class="material-icons">edit</i>\
                             </button></td>\
                             */
                        $("#transactions-content tbody").append(newRowContent);
                    });
                }
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to load transaction.', 4000);
            }
        });

        $("#customer-list").html('None');
        $('#customer-list').material_select();
        $('#customer-list').on('contentChanged', function() {
            $(this).material_select();
          });

        $("#customer-search-list").html('None');
        $('#customer-search-list').material_select();
        $('#customer-search-list').on('contentChanged', function() {
            $(this).material_select();
          });

        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "customerList",
            dataType: "json",
            success: function(data){
                var datas = data["data"];
                console.log(datas)
                $.each (datas, function(i, item) {
                    var $newOpt = $("<option>").attr("value",datas[i].customer_id).text(datas[i].customer_name);
                    $('#customer-list').append($newOpt);
                    
                });
                $("#customer-list").trigger('contentChanged');
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to load services.', 4000);
            }
        });

        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("token", $.cookie("token"));
            },
            url: baseApi + "customerList",
            dataType: "json",
            success: function(data){
                var datas = data["data"];
                console.log(datas)
                $.each (datas, function(i, item) {
                    var $newOpt = $("<option>").attr("value",datas[i].customer_id).text(datas[i].customer_name);
                    $('#customer-search-list').append($newOpt);
                    
                });
                $("#customer-search-list").trigger('contentChanged');
            },
            error: function (data) {
                var data = data;
                Materialize.toast('Failed to load services.', 4000);
            }
        });

    }
});