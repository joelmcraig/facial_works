<?php

	session_start();
	
	include ('../connection.php');
	
	if(isset($_SESSION['account_id'])){
		
		
	}else{
		header('Location: ../index.php');
	}
	
	
	
 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a modern responsive CSS framework based on Material Design by Google. ">
    <title>Facial Works</title>
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- <link rel="icon" href="../logo.png" sizes="32x32"> -->
    <!--  Android 5 Chrome Color-->
    <meta name="theme-color" content="#EE6E73">
    <!-- CSS-->
    <link href="../css/prism.css" rel="stylesheet">
    <link href="../css/ghpages-materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="http://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="//cdn.transifex.com/live.js"></script>
	
	<style>
	 /* label color */
   .input-field label {
     color: #ab47bc ;
   }
   /* label focus color */
   .input-field input[type=text]:focus + label {
     color: #ab47bc ;
   }
   /* label underline focus color */
   .input-field input[type=text]:focus {
     border-bottom: 1px solid #ab47bc ;
     box-shadow: 0 1px 0 0 #ab47bc ;
   }
   /* valid color */
   .input-field input[type=text].valid {
     border-bottom: 1px solid #ab47bc ;
     box-shadow: 0 1px 0 0 #ab47bc ;
   }
   /* invalid color */
   .input-field input[type=text].invalid {
     border-bottom: 1px solid #ab47bc ;
     box-shadow: 0 1px 0 0 #ab47bc ;
   }
   
    /* label focus color */
   .input-field input[type=password]:focus + label {
     color: #ab47bc ;
   }
   /* label underline focus color */
   .input-field input[type=password]:focus {
     border-bottom: 1px solid #ab47bc ;
     box-shadow: 0 1px 0 0 #ab47bc ;
   }
   /* valid color */
   .input-field input[type=password].valid {
     border-bottom: 1px solid #ab47bc ;
     box-shadow: 0 1px 0 0 #ab47bc ;
   }
   /* invalid color */
   .input-field input[type=password].invalid {
     border-bottom: 1px solid #ab47bc ;
     box-shadow: 0 1px 0 0 #ab47bc ;
   }
   /* icon prefix focus color */
   .input-field .prefix.active {
     color: #ab47bc ;
   }
   
   #textarea1:focus {
	  border-bottom: 1px solid #ab47bc ;
	  box-shadow: 0 1px 0 0 #ab47bc ;
	}

	#textarea1:focus + label {
	  color: #ab47bc  ;
	}

   #textarea1Edit:focus {
	  border-bottom: 1px solid #ab47bc ;
	  box-shadow: 0 1px 0 0 #ab47bc ;
	}

	#textarea1Edit:focus + label {
	  color: #ab47bc  ;
	}
	
	#textarea1Message:focus {
	  border-bottom: 1px solid #ab47bc ;
	  box-shadow: 0 1px 0 0 #ab47bc ;
	}

	#textarea1Message:focus + label {
	  color: #ab47bc  ;
	}
	</style>
	
  </head>
  <body>
    <header>
      <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only"><i class="material-icons">menu</i></a></div>
      <ul id="nav-mobile" class="side-nav fixed">
        <!-- <li class="logo" style="height:150px;">
			<a id="logo-container" style="height:150px;" href="index.php" class="brand-logo">
				<img src="../logo.png" style="height:150px;width:150px;">
			</a>
		</li> -->
        <li class="bold"><a href="index.php" class="waves-effect waves-purple">Dashboard</a></li>
        <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
			<li class="bold"><a class="collapsible-header  waves-effect waves-purple">Users</a>
              <div class="collapsible-body">
                <ul>
                  <li id="userList"><a href="#">User List</a></li>
                  <li id="userListCancel"><a href="#">Pending Registration</a></li>
                </ul>
              </div>
            </li>
            <li class="bold"><a class="collapsible-header  waves-effect waves-purple">Inventory and Sales Reports</a>
              <div class="collapsible-body">
                <ul>
                  <li id="reportsListOfIBD"><a href="#">Inventory (by day)</a></li>
                  <li id="reportsListOfIA"><a href="#">Inventory (All)</a></li>
				  <li id="reportsListOfSBD"><a href="#">Sales (by day)</a></li>
				  <li id="reportsListOfSA"><a href="#">Sales (All)</a></li>
                </ul>
              </div>
            </li>
			<li class="bold"><a class="collapsible-header  waves-effect waves-purple">Printable Reports</a>
              <div class="collapsible-body">
                <ul>
                  <li><a target="_blank" href="#">Number of Customers (By Branch)</a></li>
                  <li><a target="_blank" href="#">Total Sales (by day)</a></li>
				  <li><a target="_blank" href="#">Total Sales (All)</a></li>
				  <li><a target="_blank" href="#">Inventory (by day)</a></li>
				  <li><a target="_blank" href="#">Inventory (All)</a></li>
                </ul>
              </div>
            </li>
            <li class="bold"><a class="collapsible-header  waves-effect waves-purple">Services</a>
              <div class="collapsible-body">
                <ul>
                  <li id="servicesAddSession"><a href="#">Add new Services</a></li>
                  <li id="servicesListOfSession"><a href="#">List of Services</a></li>
                </ul>
              </div>
            </li>
			<li class="bold"><a class="collapsible-header  waves-effect waves-purple">Products</a>
              <div class="collapsible-body">
                <ul>
                  <li id="productsAdd"><a href="#">Add Products</a></li>
                  <li id="productsList"><a href="#">List Products</a></li>
                </ul>
              </div>
            </li>
            <li class="bold"><a class="collapsible-header  waves-effect waves-purple">Utilities</a>
              <div class="collapsible-body">
                <ul>
				  <li id="#"><a href="#">How to Backup and Restore</a></li>
                  <li><a href="#">Backup and Restore</a></li>
                </ul>
              </div>
            </li>
          </ul>
        </li>
        <li class="bold"><a href="../ajax/logout.php" class="waves-effect waves-purple">Logout</a></li>
      </ul>
    </header>
    <main>      <div class="section no-pad-bot purple lighten-1" id="index-banner">
        <div class="container purple lighten-1">
          <h1 class="header center-on-small-only">Facial Works</h1>
          <div class='row center'>
            <h3 class="col s12 light center header black-text">Facial Works: Record Management & Inventory System</h3>
            <h5 class="col s12 light center header black-text">The main focus of this system is to provide and assist the Record Management & Inventory System of Facial Works Skin Care Center in managing the client records, update their business transactions including their product inventories and monthly sales reports.</h5>
          </div>


        </div>
        <div class="github-commit">
          <div class="container">
            <div class="commit">
              
            </div>
          </div>
        </div>
      </div>

      <div class="container">
	  
	    <div id="log"></div>
        <div class="section" id="loader">

          <div class="row">
            
          </div>

         


    </main>    <footer class="page-footer purple lighten-1">
      <div class="container">
        <div class="row">
          
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
        © 2018 Facial Works, All rights reserved.
        </div>
      </div>
    </footer>

    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script>if (!window.jQuery) { document.write('<script src="bin/jquery-2.1.1.min.js"><\/script>'); }
    </script>
    <script src="../js/jquery.timeago.min.js"></script>
    <script src="../js/prism.js"></script>
    <script src="../jade/lunr.min.js"></script>
    <script src="../jade/search.js"></script>
    <script src="../bin/materialize.js"></script>
    <script src="../js/init.js"></script>
	<script src="../ajax/global.js"></script>
    

  </body>
</html>